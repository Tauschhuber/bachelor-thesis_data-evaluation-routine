# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 11:02:13 2020

@author: uz87ymil
"""

from Evaluation import image_processing as img
from Evaluation import evaluation as eva
from Toolbox import toolbox as tb
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import os
import time


'''
Eingabe: meinen Ordner, wieviele Kreise ich haben möchte, datashape
'''

def processing_algorithm_call(path_to_reco_folder, number_of_circles, adsorber_diameter, datashape, threshold_grey = None, beamHardeningCorrection = False):
    '''
    

    Parameters
    ----------
    path_to_reco_folder : string
        this is the path to the reco folder.
    number_of_circles : int
        the number od circles you want to plot.
    adsorber_diameter : float
        outer diameter of the adsorber. You have to measure this value on the real adsorber.
    datashape : tuple
        shape of the reconstruction data.
    threshold_grey : float or None
        the threshold you want to use for the calculation of the average grey values. If this field is left empty,
        the algorithm will apply the peak method instead of the average grey value method. The default is None.

    beamHardeningCorrection : boolean, optional
        If the boolean is True, the algorithm will do a beam hardening correction for the grey values. The default is False.

    Returns
    -------
    None.

    '''
    start_time = time.time()
    files_list = os.listdir(path_to_reco_folder)
    
    if number_of_circles > 10:
        raise ValueError("The number of circles is higher than 10. You have to insert more colours into the functions plot_grey_values and visualize_masks in order to plot your data with more than ten circles.")
        
    #-------------------------------------------------------------------------
    
    # get information about the directory
    head_path, tail = os.path.split(path_to_reco_folder)
    head_path_2, number_of_folder = os.path.split(head_path)
    
    # import the first reco data file and get masks
    
    path_to_first_file = path_to_reco_folder + "\\" + files_list[0]
    data = tb.importRecoData(path_to_first_file, header = 2048, datashape = datashape)
    
    north_point, south_point, west_point, west_coast = img.find_circle_3(data, step = 1, threshold = 0.7) # find the circle in data -> I have to do this once
    
    # save memory
    data = data[0]
    
    # calculate the circle parameters -> circle center and radius
    radius, circle_center = img.define_circle(north_point, south_point)
    
    # cut border of the adsorber
    radius_no_border = img.cut_circle_border(radius, adsorber_diameter)
    
    # create certain number of coextensive circles
    coextensive_radiuses = img.get_coextensive_radiuses(radius_no_border, number_of_circles)
    
    # set masks with these circle radiuses -> save these masks and apply them to every datafile
    circles = img.create_circle_efficient((datashape[0]), coextensive_radiuses, circle_center)
    
    # cut inner pieces of the masks
    masks = img.cut_inner_circles(circles)
    
    #-------------------------------------------------------------------------
    
    # initialize my grey value list
    list_grey_values = []
    
    # loop through all files in my folder
    
    meanZDirection = 4
    
    for pos, file_name in enumerate(files_list):
        
        path_to_my_data = path_to_reco_folder + "\\" + file_name
        current_data = tb.importRecoData(path_to_my_data, header = 2048, datashape = datashape, meanZDirection = 0)
        
        if pos < len(files_list)-1:
            path_to_my_dataPlusOne = path_to_reco_folder + "\\" + files_list[pos+1]
            current_dataPlusOne = tb.importRecoData(path_to_my_dataPlusOne, header = 2048, datashape = datashape, meanZDirection = 0)
            
        if pos > 0:
            path_to_my_dataMinusOne = path_to_reco_folder + "\\" + files_list[pos-1]
            current_dataMinusOne = tb.importRecoData(path_to_my_dataMinusOne, header = 2048, datashape = datashape, meanZDirection = 0)
        
        
        #nehme am ende das nächste file
        #achtung unschöner code folgt
        if pos == 0:
             for i in range(0, len(current_data)):
                if i < meanZDirection:
                    temp = current_data[0:(i+meanZDirection)]
                    current_data[i] = np.mean(temp, axis = 0)
                if i >= meanZDirection and i <= (len(current_data)-meanZDirection):
                    temp = current_data[(i-meanZDirection):(i+meanZDirection)]
                    current_data[i] = np.mean(temp, axis = 0)
                if i > (len(current_data)-meanZDirection):
                    temp = current_data[i-meanZDirection:]
                    temp2 = current_dataPlusOne[0:(i+meanZDirection-len(current_data))]
                    temp3 = np.concatenate((temp, temp2))
                    current_data[i] = np.mean(temp3, axis = 0)
            
        #nehme am anfang das file davor 
        elif pos == len(files_list):
             for i in range(0, len(current_data)):
                if i < meanZDirection:
                    temp = current_data[0:(i+meanZDirection)]
                    temp2 = current_dataMinusOne[-(-i+meanZDirection-len(current_data)):]
                    temp3 = np.concatenate((temp, temp2))
                    current_data[i] = np.mean(temp3, axis = 0)
                if i >= meanZDirection and i <= (len(current_data)-meanZDirection):
                    temp = current_data[(i-meanZDirection):(i+meanZDirection)]
                    current_data[i] = np.mean(temp, axis = 0)
                if i > (len(current_data)-meanZDirection):
                    temp = current_data[i-meanZDirection:]
                    current_data[i] = np.mean(temp, axis = 0)
            
        #nehme am anfang das file davor und am ende das file danach
        else:
            for i in range(0, len(current_data)):
                if i < meanZDirection:
                    temp = current_data[0:(i+meanZDirection)]
                    temp2 = current_dataMinusOne[-(-i+meanZDirection-len(current_data)):]
                    temp3 = np.concatenate((temp, temp2))
                    current_data[i] = np.mean(temp3, axis = 0)
                if i >= meanZDirection and i <= (len(current_data)-meanZDirection):
                    temp = current_data[(i-meanZDirection):(i+meanZDirection)]
                    current_data[i] = np.mean(temp, axis = 0)
                if i > (len(current_data)-meanZDirection):
                    temp = current_data[i-meanZDirection:]
                    temp2 = current_dataPlusOne[0:(i+meanZDirection-len(current_data))]
                    temp3 = np.concatenate((temp, temp2))
                    current_data[i] = np.mean(temp3, axis = 0)
        
        
        #------------ hier fängt loop über eine Rekodatei an
        
        grey_values_one_data_file = list()
        
        for i in range(len(current_data)):
            
            # set masks on data
            data_masked = img.set_mask_2(current_data[i], masks)
            
            if threshold_grey != None:                
                # calculate the average grey values of my masked data slices, returns a 1D array
                grey_values = img.calculate_grey_values_2(data_masked, threshold_grey)
                
            
            else:
                # calculate grey values based on grey value maximum
                grey_values = img.getMaxAboveThresh(data_masked)
            
            # append the grey values to my list of grey values
            grey_values_one_data_file = img.append_values(grey_values, grey_values_one_data_file)
            
        #---------- end loop
        
        list_grey_values = np.append(list_grey_values, grey_values_one_data_file)

    # reshape my grey values
    amount_of_values = int(len(list_grey_values)/number_of_circles)
    grey_values = np.reshape(list_grey_values, [amount_of_values, number_of_circles])
    
    # save my grey_values array as a file
    
    if threshold_grey != None:
        path_to_data_file = head_path + "\\" + number_of_folder + "_grey_values_threshold = " + str(threshold_grey) + "_" + str(number_of_circles) +"_circles.npy"
        
    else:
        path_to_data_file = head_path + "\\" + number_of_folder + "_peak_method_" + str(number_of_circles) +"_circles.npy"
    
    np.save(path_to_data_file, grey_values)
    
    end_time = time.time()
    print("Diese Funktion benötigt " + str(end_time - start_time) + "Sekunden.")
    
    return grey_values, data, masks, path_to_data_file

    #------------------------ visualization of data --------------------------


    
    


# call function if it is called in this script
if __name__ == "__main__":
    
    number_of_circles = 5
    datashape = (3301, 3301)
    #threshold_grey = 0.16

    
    path_to_reco_folder = r'G:\215004\reco'
    adsorber_diameter = 32.2 #40.0 in mm
    start_time = 173.39 # in seconds    
    grey_values_reshaped, data, masks, path_to_data_file = processing_algorithm_call(path_to_reco_folder,
                                                                                     number_of_circles,
                                                                                     adsorber_diameter,
                                                                                     datashape,                                                                                    
                                                                                     beamHardeningCorrection = False)

    
    

    
    # test = grey_values_reshaped[100:200][:]
    # test1 = np.mean(test, axis = 0)
    # testfactor = test1/test1[-1]
    # test2 = test/testfactor

    
    
    #eva.visualize_masks(path_to_reco_folder, data, masks)



"""
---To Dos----------------------------------------------------------------------

- histogram


"""