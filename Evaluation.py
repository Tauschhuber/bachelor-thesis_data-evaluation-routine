# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 13:57:03 2020

@author: uz87ymil
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import matplotlib.colors as mcolors

class image_processing:
    
    def create_circle(n, radius, circle_center):
            '''
            This function creates one or multiple circles with 1s in a matrix of 0s.
            The amount of circles created is defined by the number of elements in the
            array named radius.
            
            Parameters
            ----------
            n : length of matrix in one dimension
            radius: int or array of ints
                    radius of circle I want to create
            circle_center: x- and y- coordinated of circle center [y,x]
        
            Returns
            -------
            mask :  2D array of integers
                    one or more matrix with circle of ones and zeros outside
        
            '''
            radius = np.array(radius)
            mask = np.zeros((radius.size, n, n), dtype = int)
            
            
            if radius.size == 1: #oder isinstance(radius, int)
                for y in range(len(mask[0])):
                        for x in range(len(mask[0])): #walks through every line in the matrix
                            x_distance = x - circle_center[1] #distance in x direction (to the right)
                            y_distance = y - circle_center[0]
                            d = np.sqrt(x_distance**2 + y_distance**2) #calculate the distance of (x,y) to circle center
                            
                            if d <= radius:
                                mask[0][y][x] = 1 #y gibt Zeile an, x gibt Spalte an; y von oben nach unten, x von links nach rechts gezählt
                
            else:
                for i in range(radius.size):
                    for y in range(len(mask[i])):
                        for x in range(len(mask[i])): #walks through every line in the matrix
                            x_distance = x - circle_center[1] #distance in x direction (to the right)
                            y_distance = y - circle_center[0]
                            d = np.sqrt(x_distance**2 + y_distance**2) #calculate the distance of (x,y) to circle center
                            
                            if d <= radius[i]: # Problem, wenn radius array nur ein Element enthält
                                mask[i][y][x] = 1 #y gibt Zeile an, x gibt Spalte an; y von oben nach unten, x von links nach rechts gezählt
            return mask
#-----------------------------------------------------------------------------


    def create_circle_efficient(length, radius, center):
        
        '''
        This function creates a circle in an efficient way.
        
        '''
        maskreturn = list()
        
        if type(radius) == float or type(radius) == int:
            Y, X = np.ogrid[:length, :length]
            dist_from_center = np.sqrt((X - center[1])**2 + (Y-center[0])**2)
            maskreturn = dist_from_center <= radius
            # turn boolean into int with this little trick
            maskreturn = 1 * maskreturn
            
        else:
            maskreturn = np.zeros([len(radius), length, length], dtype = int)
            for i in range(len(radius)):
                Y, X = np.ogrid[:length, :length]
                dist_from_center = np.sqrt((X - center[1])**2 + (Y-center[0])**2)
                maskreturn[i] = dist_from_center <= radius[i]
                maskreturn[i] = 1 * maskreturn[i]
        
        return maskreturn
    
    # masks = create_circle_efficient(2000, [400, 200, 100], [1000,800])
    
    # for i in range(len(masks)):
    #     plt.figure()
    #     plt.imshow(masks[i])
    
    # mask = create_circle_efficient(2000, 100, [1000,800])
    # plt.imshow(mask)

#-----------------------------------------------------------------------------

    def cut_inner_circles(masks):
        
        '''
        Cut the inner circles of the masks.
        
        
        '''
        # Was machen, wenn die masks nur aus einer Maske besteht?
        cut_out_masks = ((np.zeros(masks.shape, dtype = int)))
        for i in range(len(masks)-1):
            cut_out_masks[i] = masks[i] - masks[i+1]
        cut_out_masks[len(masks)-1] = masks[len(masks)-1]
            
        return(cut_out_masks)
    
    # masks = eva.create_circle_efficient(1000, [200, 100 ,50], [400, 700])
    
    # cut_out_masks = cut_inner_circles(masks)
    # for i in cut_out_masks:
    #     plt.figure()
    #     plt.imshow(i)

#-----------------------------------------------------------------------------

    def find_circle(m, step = 1, threshold = 0.7):

        k = 0 # variable helps me to know whether I have already found the circle
    
        for i in range (0, len(m), step):
            
            maxIndex = np.argmax(m[i])
            '''argmax returns the index of the first
               maximum value in array (starts counting on the left)'''
               
            if k == 0 and m[i][maxIndex] >= threshold:
                k = 1 #variable to know whether circle has been found
                print('Kreis gefunden')
                north_point = [i, maxIndex]# top point of the circle
                print('nördlichster Punkt bei:', north_point) # give me coordinates of circle outline on left side
            
            # collect contour shape of circle, find most west point of the circle
            if k == 1 and np.amax(m[i]) >= threshold:
                print(i, maxIndex)
                # wenn der y-zeilenwert die ganze Zeit 0 bleibt -> gebe Fehler an
                #print('Kreis')
                
            
            # stop when for-loop leaves circle (maximum values will be below threshold)
            if k == 1 and np.amax(m[i]) < threshold:
                    print('außerhalb von Kreis')
                    south_point = [i-1, np.argmax(m[i-1])] # bottom point of the circle
                    print('südlichster Punkt bei:', south_point)
                    break
                
        return north_point, south_point
    
#find_circle(data)

#-----------------------------------------------------------------------------    

    def find_circle_2(m, step = 1, threshold = 0.7):
    
        
        for i in range(0, len(m[0]), step):
                
            maxIndex = np.argmax(m[0][i])
            '''argmax returns the index of the first
                   maximum value in array (starts counting on the left)'''
                   
            if m[0][i][maxIndex] >= threshold:
                    
                print('Kreis gefunden')
                north_point = [i, maxIndex]# top point of the circle
                print('nördlichster Punkt bei:', north_point) # give me coordinates of circle outline on left side
                start_loop_2 = north_point[0] + 1
                break
        
        array = [north_point] #create list that saves shape of circle
            
        for i in range(start_loop_2, len(m[0]), step):
                  
            maxIndex = np.argmax(m[0][i])
            # collect contour shape of circle, find most west point of the circle
            if np.amax(m[0][i]) >= threshold:
                
                array = np.append(array, [i, maxIndex]) #appends coordinates of outer circle values to the array
                print(array)
                
                # wenn der y-zeilenwert die ganze Zeit 0 bleibt -> gebe Fehler an
                
            # stop when for-loop leaves circle (maximum values will be below threshold)
            if np.amax(m[0][i]) < threshold:
                
                #get the coordinates of westernmost point in circle
                #the minimum distance from left border to the circle = westernmost point
                #in order to do that I need to reshape my array
                
                number_of_values = int(array.size/2) #reshape my array
                array = np.reshape(array, (number_of_values, 2))
                print(array)
                get_min_array = array.transpose() #array
      
                min_distance = np.argmin(get_min_array[1]) #returns the index of min distance entry
                
                west_point = list(array[min_distance])
                
                print('außerhalb von Kreis')
                south_point = [i-1, np.argmax(m[0][i-1])] # bottom point of the circle
                print('südlichster Punkt bei:', south_point)
                break
                    
        return north_point, south_point, array, west_point

#north, south, array, west_point = find_circle_2(circle)
#-----------------------------------------------------------------------------    

    def find_circle_3(data, step = 1, threshold = 0.9):
        '''
        find_circle finds the border of the adsorber (= circle) and defines its shape.
        It gives out the coordinates of the most north point, the most south point
        and the most west point.
        It also gives out the shape of the west boarder -> west_coast.
    
        Parameters
        ----------
        data : array of floats
            matrix of values between 0 and 1 -> represent picture
        step : float or int, optional
            stepwidth for approximately finding the circle. The default is 1.
        threshold : float, optional
            threshold light value, where my adsorber wall starts. The default is 0.6.
    
        Returns
        -------
        north_point : tuple
            coordinates of the northernmost point of the circle.
        south_point : tuple
            coordinates of the southernmost point of the circle.
        west_point : tuple
            coordinates of the westernmost point of the circle.
        west_coast : array of int (length, 2)
            coordinates of the western border of the circle.
        
        '''
            
        north_point = None
       
        for i in range(0, len(data[0]), step): #goes through 1D lines in array
                    
            maxIndex = np.argmax(data[0][i])
            '''argmax returns the index of the first
                    dataaxidataudata value in array (starts counting on the left),
                    once this threshold is surpassed, I have found datay adsorber'''
                   
            if data[0][i][maxIndex] >= threshold:
                    
                #print('Kreis gefunden')
                start_point = [i, maxIndex] # top point of the circle
                #print('Startpunkt bei:', start_point) # give data coordinates of circle outline on left side
                start_loop_1 = start_point[0] + 1
                break
                
        #while north_point == None:
        for j in range(start_loop_1 -20, len(data[0])): #goes through matrix lines
            
            for k in range(len(data[0][0])):
    
                if data[0][j][k] >= threshold:
                    #print('Kreis mit zweitem Loop gefunden')
                    north_point = (j, k) # top point of the circle
                    print('nördlichster Punkt bei:', north_point)
                    west_coast = np.array(north_point) #create tuple that saves shape of circle westside
                    start_loop_2 = north_point[0]
                    break
            if north_point != None: #goes into next for-loop
                break    
            
        # collecting contour shape of circle, find most west point of the circle        
        for j in range(start_loop_2, len(data[0])): #goes through matrix lines
            flag = 0        
            for k in range(len(data[0][0])):
                  
                if data[0][j][k] >= threshold:
                    #print(k)
                    west_coast = np.append(west_coast, (j, k))
                    #appends coordinates of outer circle values to the array
                    flag = 1
                    break
            if flag == 0: # in case none of the values surpasses the threshold ->
                #print("Kreisende gefunden.")
                south_point = (j-1, np.argmax(data[0][j-1])) # most south point of the circle
                print('südlichster Punkt bei:', south_point)
                break
                                
        #if I have shape of circle -> get coordinates of westernmost point in circle
        #in order to do that I need to reshape datay array
            
        number_of_values = int(west_coast.size/2) #reshape data west_coast
        west_coast = np.reshape(west_coast, (number_of_values, 2))
        get_min_west_coast = west_coast.transpose() #west_coast
        #the minimum distance from left border to the circle = westernmost point
        index_min_distance = np.argmin(get_min_west_coast[1]) # returns the indices of the minimum values along an axis
            
        west_point = tuple(west_coast[index_min_distance])
        print("westlichster Punkt bei:", west_point)
                    
        return north_point, south_point, west_point, west_coast

#------------------------------------------------------------------------------
    def visualize_circle(dataset):
        '''
        function transposes dataset and visualizes it
        
        dataset: 2d dimensional array in the size (x, 2)
        
        '''
        dataset = dataset.T
        x = dataset[0]
        y = dataset[1]
        plt.plot(y,x)
        plt.title('west border of circle')
        plt.xlabel("pixels in x-direction")
        plt.ylabel("pixels in y-direction")
        plt.axis('square')

#------------------------------------------------------------------------------

    def define_circle(north_point, south_point):
        '''
        Parameters
        ----------
        north_point : x,y coordinates of top point of circle
        south_point : x,y coordinates of bottom point of circle
    
        Returns
        -------
        radius : radius of circle
        circle_center : x,y coordinates of circle center
    
        '''
    
        radius = (south_point[0] - north_point[0])/2
        x_center = (south_point[1]+ north_point[1])/2
        y_center = north_point[0] + radius
        circle_center = [y_center, x_center]
        
        print("The adsorber has a radius of", radius, "pixel.")
        print("The adsorber center lies at ", circle_center)
        
        return radius, circle_center

#------------------------------------------------------------------------------

    def check_circle_values(north_point, south_point, west_point, radius, deviation = 10):
        
        # check if west_point y-value lies in the middle between south and 
        y_west_calculated = north_point[0] - radius
        if y_west_calculated - west_point[0] > deviation:
            raise ValueError("The calculated west point value deviates more than", deviation, "pixels.")
        return

#-----------------------------------------------------------------------------
    
    def cut_circle_border(radius, adsorber_outer_diameter, adsorber_border = 1.1):
        '''
        This function reduces the given radius by the border of the adsorber, so we
        can set a mask to the matrix without the border of the adsorber.
    
        Parameters
        ----------
        radius : int
            outer radius of circle in pixel, with the border of the adsorber
        adsorber_border : float
            thickness of adsorber border in real life in mm
        adsorber_outer_diameter : 
            diameter of the adsorber in real life in mm
    
        Returns
        -------
        radius_no_border : float or int
            radius of the circle without the border in pixel 
    
        '''
        
        # transform adsorber_border to adsorber border in pixels
        transformation_factor = radius*2/adsorber_outer_diameter
        circle_border_pixel = adsorber_border * transformation_factor
        
        # subtract border from radius and subtract an additional 10 pixel
        radius_no_border = radius - circle_border_pixel - 30
        
        return radius_no_border

#-----------------------------------------------------------------------------

    def get_coextensive_radiuses(outer_radius, number_of_circles):
        '''
        This function calculates the radiuses for a number of coextensive circles
        inside a circle with the outer radius r_a.
    
        Parameters
        ----------
        outer_radius : int or float
            radius of outer circle
        number_of_circles : int
            number of coextensive circles I want to create
    
        Returns
        -------
        radiuses : array of floats or ints
            radiuses of my coextensive circles, starting from the outermost radius
    
        '''
        
        # check types and raise errors I can learn from
        if type(number_of_circles) != int:
            raise TypeError("number_of_circles must be an integer.")
                
        radiuses = np.zeros(number_of_circles)
        n = number_of_circles
        
        for i in range(number_of_circles):
        # create coextensive circles
            A_a = outer_radius**2 *np.pi # the complete circle area
            A_border = A_a/n # outer ring area
            r_i = np.sqrt(1/np.pi*(A_a - A_border)) # calculate radius of inner circle
            radiuses[i] = outer_radius
            n = n - 1
            outer_radius = r_i
        
        return radiuses
    
    # radiuses = coextensive_circles(5, 2)
    # print(radiuses)

#-----------------------------------------------------------------------------

    def set_mask(data, radiuses, circle_center):
        '''
        This function sets a mask with a given radius and multiplies it with the 
        data. Like this, I can look at different circle segments.
    
        Parameters
        ----------
        data : array of floats
            array with my data
        radius : int
            radius of mask I wish to set
        circle_center : array of int
            [y, x] coordinates of center of mask I want to set
    
        Returns
        -------
        data_masked : TYPE
            DESCRIPTION.
    
        '''
        mask = eva.create_circle(len(data[0]), radiuses, circle_center)
        data_masked = data * mask # oder np.multiply
        # alle Werte, die null sind, rauswerfen
        
        return data_masked
    
#-----------------------------------------------------------------------------

    def set_mask_2(data, masks):
        '''
        This function sets one or multiple masks and multiplies it with the 
        data. Like this, I can look at different circle segments.
    
        Parameters
        ----------
        data : array of floats
            array with my data
        masks : one 2D arrays of ints or 3d array of ints
                containing multiple 2D arrays of ints
                one or more matrixes used as masks
    
        Returns
        -------
        data_masked : one 2D array of ints or 3D array of ints containing
                      multiple 2D arrays of ints
            the masked data
    
        '''

        data_masked = np.multiply(data, masks) # oder np.multiply
        # alle Werte, die null sind, rauswerfen
        
        return data_masked

#-----------------------------------------------------------------------------

    def clean_noise(matrix):
        '''
        This function cleanes the noise in the computertomograpghy data. It sets
        grey values below zero to zero and values above one to one.
    
        Parameters
        ----------
        matrix : array of floats or integers
                the computertomography data I want to clean up
    
        Returns
        -------
        matrix : array of floats or integers
                the cleaned up data, only containing values from zero to one
    
        '''
        matrix[matrix < 0] = 0
        matrix[matrix > 1] = 1
        
        
        return matrix

#-----------------------------------------------------------------------------

    def calculate_grey_values(data):
        '''
        This function gives back the average grey value of a set of data in a matrix.
    
        Parameters
        ----------
        data : matrix of floats or uints
            a matrix of values
    
        Returns
        -------
        average_grey_value : float
            the average grey value
    
        '''
        average_grey_value = data[data>0].mean() # not taking the values equal zero into the calculation
        return average_grey_value
    
    # test
    # data = np.arange(10)
    # value = calculate_grey_values(data)
    
    # print(data)
    # print(value)
    # print(data.mean()) # all good

#------------------------------------------------------------------------------

    def calculate_grey_values_2(data, threshold_grey = 0.1):
        '''
        This function gives back the average grey value of a set of data in a 
        3D array. The array has to be three-dimensional.
        The length of the input array in the third dimension determines the number
        of elements in the output array. It considers only datapoints with
        a grey value higher than the threshold.
    
        Parameters
        ----------
        data : 3D array of floats or ints
            the data from which I want to calculate the average grey value per
            2D array
    
        Returns
        -------
        average_grey_value : 1D array of floats
            the average grey values of my masked data slices
    
        '''
        average_grey_values = np.zeros([len(data),])
        
        for i in range(len(data)): # range starts at 0 and goes till (end_value - 1)
            data_file = data[i]
            average_grey_values[i] = data_file[data_file> threshold_grey].mean() # not taking the values equal zero into the calculation
    
        return average_grey_values
        
#-----------------------------------------------------------------------------    
    
    def getMaxAboveThresh(data, thresholdBin = 56):
        average_grey_values = np.zeros([len(data),])
        
        for i in range(len(data)): # range starts at 0 and goes till (end_value - 1)
            noInBins, bins = np.histogram(data[i], bins = 256)
            average_grey_values[i] = bins[thresholdBin + np.argmax(noInBins[thresholdBin:])] # not taking the values equal zero into the calculation
            
        return average_grey_values

#-----------------------------------------------------------------------------

    def append_values(new_grey_values, list_grey_values):
        '''
        
        Parameters
        ----------
        new_grey_values : array of floats
            array of grey values gained from the current picture.
        list_grey_values : list of floats, shape = [number_of_circles, length]
            This is a list of arrays.
    
        Returns
        -------
        list_grey_values : list of floats, 1 d numpy array
            This is an array of my grey values.
    
        '''
        
        list_grey_values = np.append(list_grey_values, new_grey_values)
        
        return list_grey_values
    
    # testing
    # liste = [[1, 2, 3], [2, 3, 6], [3, 6, 7]]
    # x= np.array([4, 8, 10])
    
    # new_liste = append_values(x, liste)

#-----------------------------------------------------------------------------



class evaluation:

    def plot_grey_values_2(numpy_array, file_path, start_value, number_of_circles, ymin, ymax):
        
        '''
        This function plots the grey values given in an array and visualizes the
        data. Plots a pretty picture of my values.
        
            Parameters
            ----------
            numpy_array : array of float
               array with grey values
            number_of_circles : int
                number of circles I used in my calculation
            start_value : float
                time where the x-axis of the plot starts in minutes
            file_path : string
                path to the numpy array I want to visualize
        
        '''
        # get start value in minutes
        start_value = start_value/60
        x_axis = np.arange(start_value, numpy_array.shape[0] * 12/60 + start_value, 12/60) # get the timeline
        
        #plt.figure() #create the figure
        
        colours = ["r", "g", "b", "m" , "c", "k", "y"]
        symbols = ["o", "s", ">", "p", "^", "D", "*", "<", "H"]
        
        # plot the grey values
        for i in range(number_of_circles):
            plt.plot(x_axis, numpy_array[:, i], colours[i] + symbols[i]) # use special colours
        
        # adapt y-axis
        axes = plt.gca()
        axes.set_ylim([ymin, ymax])
        
        # plot legend depending on how many circles I have
        legend_handles = evaluation.get_legend(number_of_circles)
        
        if not legend_handles == []: # if legend_handles is not empty, plot a legend
            plt.legend(legend_handles, loc = 4, fontsize = 18)

        #ploteinstellungen
        
        plt.xlabel('time since start of measurement/ min', size = 18)
        plt.ylabel('grey values / -', size = 18)
        plt.tick_params(axis='both', which='major', labelsize=16)
        
        plt.savefig(file_path + ".png", dpi = 300, bbox_inches = "tight")
        
        
        return
        
        # testing
        #plot_numpy_array(r"C:\Users\uz87ymil\Documents\Maikes Bachelorarbeit\Testspace\215006_numpy_array.npy", 0, 3)

        # #fullscreen
        # figManager = plt.get_current_fig_manager()
        # figManager.window.showMaximized()

#------------------------------------------------------------------------------

    def visualize_masks(path_to_reco_folder, data, masks):
       
        '''
        This function visualizes the masks over the data.
    
        Parameters
        ----------
        path_to_reco_folder : string
            path to the folder where I want to save my image.
        data : array of floats
            my data from the adsorber.
        masks : 3 dimensional array
            masks for my data.
    
        Returns
        -------
        None.
    
        '''
        
        # get information about the directory
        
        head_path, tail = os.path.split(path_to_reco_folder) # tail should be reco
        head_path_2, number_of_folder = os.path.split(head_path)
        
        # create the colourmaps for my plot
        
        cmap_array = []
    
        colours_red = [(1,0,0,c) for c in np.linspace(0,1,100)]
        cmap_red = mcolors.LinearSegmentedColormap.from_list('mycmap', colours_red, N=5)
        colours_green = [(0,1,0,c) for c in np.linspace(0,1,100)]
        cmap_green = mcolors.LinearSegmentedColormap.from_list('mycmap', colours_green, N=5)
        colours_blue = [(0,0,1,c) for c in np.linspace(0,1,100)]
        cmap_blue = mcolors.LinearSegmentedColormap.from_list('mycmap', colours_blue, N=5)
        colours_magenta = [(1,0,1,c) for c in np.linspace(0,1,100)]
        cmap_magenta = mcolors.LinearSegmentedColormap.from_list('mycmap', colours_magenta, N=5)
        colours_cyan = [(0,1,1,c) for c in np.linspace(0,1,100)]
        cmap_cyan = mcolors.LinearSegmentedColormap.from_list('mycmap', colours_cyan, N=5)
        colours_black = [(0,0,0,c) for c in np.linspace(0,1,100)]
        cmap_black = mcolors.LinearSegmentedColormap.from_list('mycmap', colours_black, N=5)
        colours_yellow = [(1,1,0,c) for c in np.linspace(0,1,100)]
        cmap_yellow = mcolors.LinearSegmentedColormap.from_list('mycmap', colours_yellow, N=5)
    
        cmap_array = [cmap_red, cmap_green, cmap_blue, cmap_magenta, cmap_cyan, cmap_black, cmap_yellow]
    
    
        plt.figure()
        plt.imshow(data, cmap = "gray")
    
        # plot masks on top of my data
        
        for i in range(len(masks)):
            plt.imshow(masks[i], cmap = cmap_array[i])
    
        plt.savefig(head_path + "\\" + number_of_folder + "_masks", dpi = 300)
        
    
        return
#-----------------------------------------------------------------------------
        
    def get_legend(number_of_circles):
 
        '''
        This function gets a legend handle depending on the number of circles plotted.
        
            Parameters
            ----------
            number_of_circles : int
                number of circles you are plotting
        
            Returns
            -------
            legend_handles : list
                legend description for your plot
        '''
        
        legend_handles = []
        
        if number_of_circles > 3:
    
            for i in range(number_of_circles):
                legend_handles.append("circle " + str(i+1))
                
        if number_of_circles == 3:
            legend_handles = ["outer ring", "middle ring", "inner circle"]
    
        if number_of_circles == 2:
            legend_handles = ["outer ring", "inner circle"]
    
        if number_of_circles == 1:
            legend_handles = []
            
        return legend_handles
    
    
    # testing the function
    
    #number_of_circles = 2
    #legend_handles = get_legend(number_of_circles)
    #print(legend_handles)
            
#-----------------------------------------------------------------------------
    def calculate_offset(start_time, start_value):
        
        '''
        This function calculates the offset between start_time and start_value
        or end_value you need in order to cut the data.
        
        Parameters
        ----------
        start_time : float
            in seconds
        start_value : int
            in seconds
    
        Returns
        -------
        adapted_start_value : int
            this is where the algorithm will cut the beginning of your data array.
        '''
        
        offset = start_value - start_time # in seconds -> needs to be adapted to number of datapoints
        adapted_start_value = int(offset/12)
        
        return adapted_start_value
        
    # testing
    # offset = calculate_starttime_offset(123.56, 15)
    # print(offset)

#-----------------------------------------------------------------------------
    def cut_array(numpy_array, start_value, end_value):
        
        '''
        
        This function is used to cut off the redundant data at the end of a numpy array and gives back
        a numpy array of the desired length .
        
        Parameters
        ----------
        numpy_array : array
            this is the numpy array you want to manipulate. shape: (number_of_datapoints, number_of_circles)
        end_value : int
            this is the end value that equals the desired length of your data.
    
    
        Returns
        -------
        cut_numpy_array : array
            this is your numpy array without the redundant data.
        
        '''    
        
        # cut the array
        if start_value != None:
            numpy_array = numpy_array[start_value : , :]
        if end_value != None:
            numpy_array = numpy_array[: end_value, :]
            
        
        return numpy_array

#-----------------------------------------------------------------------------

    def get_standardize_factors(numpy_array):
        '''
        This function calculates the factors to standardize the data. Standardizes
        the data towards the mean of the highest values in the array.
    
        Parameters
        ----------
        numpy_array : TYPE
            DESCRIPTION.
    
        Returns
        -------
        factors : numpy array
            factors to multiply the data with.
    
        '''
        
        mean_values = np.mean(numpy_array[200:], axis = 0) # axis=0 means it will calculate the mean over the columns
        highest_mean_value = mean_values[0] # the outermost ring has the highest values
        factors = np.ones(numpy_array.shape[1])
        for i in range(len(factors)):
                       factors[i] = highest_mean_value/mean_values[i]
        
        return factors
        
#-----------------------------------------------------------------------------

    def standardize_data(numpy_array, factors):
        '''
        This function standardizes the values in the numpy array with a given set
        of factors. The first factor will multiply the first column, the second
        factor the second column and so on.
    
        Parameters
        ----------
        numpy_array : numpy array of floats in the shape [number of values, number of circles]
            data you want to adapt.
        factors : numpy array of floats
            the factors to correct beam hardening.
    
        Returns
        -------
        standardized_data : numpy array of floats
            the standardized data.
    
        '''
    
        standardized_data = numpy_array * factors
        print("Your data has been successfully standardized.")
        return standardized_data

#-----------------------------------------------------------------------------

    def analyze_grey_values(input_data, current_path):
        '''
        This function plots the values in the given data into a histogram.
    
        Parameters
        ----------
        input_data : TYPE
            DESCRIPTION.
    
        Returns
        -------
        None.
    
        '''    
        
        plt.hist(input_data, bins = 100)
    
        plt.xlabel("Grauwerte")
        plt.ylabel("Anzahl Pixel")
        plt.savefig(current_path + "histogramm.png")
        
        return
