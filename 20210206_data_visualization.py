# -*- coding: utf-8 -*-
"""
Created on Tue Feb 16 14:21:35 2021

@author: Peter
"""

from Evaluation import evaluation as eva
import numpy as np
import matplotlib.pyplot as plt
import os
import matplotlib.colors as mcolors

def data_evaluation(path_to_numpy_file, start_time, number_of_circles, ymin, ymax, beam_hardening = False, start_value = None, end_value = None):
    
    '''
    This function is used to cut off the redundant data at the end of a numpy array and gives back
    a numpy array of the desired length.
    
    Parameters
    ----------
    path_to_numpy_file : string
        path to the numpy array you want to plot.
    start_time : float
        number of seconds between switching the gas stream and starting the measurement.
    number_of_circles : int
        the number of circles you plotted on your adsorber.
    start_value : int
        starting time of your plot in minutes. The data is cut to this starting point.
    end_value : int
        defines until how many minutes do you want to plot your data


    Returns
    -------
    None.
        
    '''    
    
    # check start_value
    if start_value != None:
        start_value = start_value *60
        if start_value < start_time:
            raise ValueError("The given start value is smaller than the start time. The start value for the plot must be higher than the start time of the experiment divided through 60.")
            adapted_start_value = 0
        else:
            adapted_start_value = eva.calculate_offset(start_time, start_value)
    else:
        start_value = start_time
        adapted_start_value = 0
    
    # reading in the numpy array
    head, file_name_with_ending = os.path.split(path_to_numpy_file)
    if file_name_with_ending[-3:] != "npy":
        raise TypeError("The given path does not lead to a numpy file. Be sure to include the file ending .npy.")
    
    file_name = file_name_with_ending[0:-4]
    
    numpy_array = np.load(path_to_numpy_file)
    
    # correct beam hardening
    if beam_hardening == True:
        factors = eva.get_standardize_factors(numpy_array)
        numpy_array = eva.standardize_data(numpy_array, factors)
    
    if end_value != None:
        
        # adapt end value
        end_value = 60 * end_value
        adapted_end_value = eva.calculate_offset(start_value, end_value)
    else:
        adapted_end_value = None
        
    # cut the numpy array
    numpy_array = eva.cut_array(numpy_array, adapted_start_value, adapted_end_value)
    
    # plot the values and save the plot

    file_path = head + "\\" + file_name + "_beam_hardening_" + str(beam_hardening)
    eva.plot_grey_values_2(numpy_array, file_path, start_value, number_of_circles, ymin, ymax)
    

# testing
plt.close("all")
start_time = 392.56
path_to_numpy_file = r"G:\215002\numpy arrays\215002_peak_method_1_circles.npy"

number_of_circles = 1
start_value = 7 # in min
end_value = 60
ymin = 0.24 # y-axis
ymax = 0.36
beam_hardening = True

data_evaluation(path_to_numpy_file, start_time, number_of_circles, ymin, ymax, beam_hardening, start_value, end_value)

# path_to_numpy_file = r"G:\215002\215002_peak_method_1_circles.npy"
# number_of_circles = 1

data_evaluation(path_to_numpy_file, start_time, number_of_circles, ymin, ymax, beam_hardening, start_value, end_value)








